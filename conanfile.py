from conans import ConanFile, tools
import shutil

class NanobenchConan(ConanFile):
    name           = "nanobench"
    version        = "4.3.0"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-nanobench/"
    homepage       = "https://github.com/martinus/nanobench"
    description    = "Simple, fast, accurate single-header microbenchmarking functionality for C++11/14/17/20 https://nanobench.ankerl.com"
    topics         = ("benchmark", "microbenchmark", "header-only")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/martinus/nanobench/archive/v{}.zip".format(self.version))
        shutil.move("nanobench-{}".format(self.version), "nanobench")

    def package(self):
        self.copy("*.h", "include", src="nanobench/src/include")

    def package_info(self):
        self.info.header_only()
